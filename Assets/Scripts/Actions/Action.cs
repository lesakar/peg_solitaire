﻿using System.Collections;

public abstract class Action 
{
	public abstract void Perform();
	public abstract void Undo();
}
