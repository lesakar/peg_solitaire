﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class MovePegAction : Action 
{
	private Peg _peg;
	private Peg _removedPeg;
	private Board _board;
	private CellCoordinates _oldCoordinates;
	private CellCoordinates _newCoordinates;

	public MovePegAction(Peg peg, Board board, CellCoordinates newCoordinates)
	{
		_peg = peg;
		_board = board;
		_oldCoordinates = new CellCoordinates(_peg.Coordinates);
		_newCoordinates = newCoordinates;
	}

	public override void Perform()
	{
		// Find peg in between peg that is moving and destination, then remove it
		CellCoordinates pegToRemoveCoordinates = _newCoordinates - _peg.Coordinates;
		pegToRemoveCoordinates.X /= 2;
		pegToRemoveCoordinates.Z /= 2;

		_removedPeg = _board.RemovePeg(_peg.Coordinates + pegToRemoveCoordinates);

		_board.OnPegMoved(_peg, _newCoordinates);
	}

	public override void Undo()
	{
		_board.RestorePeg(_removedPeg);
		_board.OnPegMoved(_peg, _oldCoordinates);
	}
}
