﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	[SerializeField] private Board _board;
	[SerializeField] private LevelController _levelController;
	[SerializeField] private float _doubleTapDelay = 0.3f;

	private Peg _selectedPeg;
	private CellCoordinates _mouseCellCoordinates;
	private float _lastTapTime = 0;

	private void Update()
	{
		if (Application.isMobilePlatform 
			&& Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began
			&& !EventSystem.current.IsPointerOverGameObject())
		{
			_mouseCellCoordinates = GetCoordinatesAtPosition(Input.GetTouch(0).position);

			if (Time.time < _lastTapTime + _doubleTapDelay)
			{
				OnDoubleTap();
			}
			else
			{
				OnTap();
			}
			_lastTapTime = Time.time;
		}
		else if (!Application.isMobilePlatform && !EventSystem.current.IsPointerOverGameObject())
		{
			CellCoordinates mouse = GetCoordinatesAtPosition(Input.mousePosition);
			if (mouse != _mouseCellCoordinates)
			{
				OnHoverExit();
				_mouseCellCoordinates = mouse;
				OnHoverEnter();
			}

			if (Input.GetMouseButtonDown(0))
			{
				OnDoubleTap();
			}
		}
	}

	#region Input events
	private void OnTap()
	{
		Cell cell = _board.GetCellAtCoordinates(_mouseCellCoordinates);
		_board.SetMoveablePegsVisible(cell != null);
	}

	private void OnDoubleTap()
	{
		Cell cell = _board.GetCellAtCoordinates(_mouseCellCoordinates);
		if (cell != null)
		{
			if (_selectedPeg == null && cell.Peg != null && cell.Peg.CanMove(_board))
			{
				_selectedPeg = cell.Peg;
				_board.OnPegSelected(_selectedPeg);
			}
			else if (_selectedPeg != null && cell.Peg != _selectedPeg && _selectedPeg.IsValidCell(cell.Coordinates))
			{
				_levelController.PerformAction(new MovePegAction(_selectedPeg, _board, cell.Coordinates));
				_selectedPeg = null;
				_mouseCellCoordinates = null;
			}
			else if (_selectedPeg != null && cell.Peg == _selectedPeg)
			{
				_board.OnPegDeselected(_selectedPeg);
				_selectedPeg = null;
			}
		}
	}

	private void OnHoverEnter()
	{
		Cell cell = _board.GetCellAtCoordinates(_mouseCellCoordinates);
		if (_selectedPeg == null && cell != null && cell.Peg != null)
		{
			_board.SetMoveablePegsVisible(true);
		}
	}

	private void OnHoverExit()
	{
		Cell cell = _board.GetCellAtCoordinates(_mouseCellCoordinates);
		if (cell != null && cell.Peg != null)
		{
			_board.SetMoveablePegsVisible(false);
		}
	}
	#endregion

	private CellCoordinates GetCoordinatesAtPosition(Vector3 screenPosition)
	{
		Plane gridPlane = new Plane(Vector3.up, Vector3.zero);
		Ray ray = Camera.main.ScreenPointToRay(screenPosition);
		float hitDist = 0;
		if (gridPlane.Raycast(ray, out hitDist))
		{
			return CellCoordinates.FromWorldPosition(ray.GetPoint(hitDist));
		}

		return null;
	}
}
