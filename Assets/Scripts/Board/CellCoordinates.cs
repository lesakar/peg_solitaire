﻿using UnityEngine;

[System.Serializable]
public class CellCoordinates 
{
	public int X { get; set; }
	public int Z { get; set; }
	public int Y { get { return -X - Z; } }

	public CellCoordinates(int x, int z)
	{
		X = x;
		Z = z;
	}

	public CellCoordinates(CellCoordinates other)
	{
		X = other.X;
		Z = other.Z;
	}

	public static CellCoordinates operator +(CellCoordinates a, CellCoordinates b)
	{
		return new CellCoordinates(a.X + b.X, a.Z + b.Z);
	}

	public static CellCoordinates operator -(CellCoordinates a, CellCoordinates b)
	{
		return new CellCoordinates(a.X - b.X, a.Z - b.Z);
	}

	public static bool operator ==(CellCoordinates a, CellCoordinates b)
	{
		if (ReferenceEquals(a, b))
			return true;

		if ((object)a == null || (object)b == null)
			return false;

		return a.Equals(b);
	}

	public static bool operator !=(CellCoordinates a, CellCoordinates b)
	{
		return !(a == b);
	}

	public override int GetHashCode()
	{
		int hash = 17;
		hash = hash * 23 + X;
		hash = hash * 23 + Y;
		hash = hash * 23 + Z;
		return hash;
	}

	public override bool Equals(object obj)
	{
		CellCoordinates other = (CellCoordinates)obj;
		return X == other.X && Z == other.Z;
	}

	public override string ToString()
	{
		return string.Format("[{0}, {1}, {2}]", X, Y, Z);
	}

	#region Static helpers
	public static CellCoordinates FromOffsetCoordinates(int x, int z)
	{
		return new CellCoordinates(x - (z / 2), z);
	}

	public static CellCoordinates FromWorldPosition(Vector3 position)
	{
		float x = position.x / (Board.InnerRadius * 2f);
		float y = -x;
		float offset = position.z / (Board.OuterRadius * 3f);
		x -= offset;
		y -= offset;

		int iX = Mathf.RoundToInt(x);
		int iY = Mathf.RoundToInt(y);
		int iZ = Mathf.RoundToInt(-x - y);

		if (iX + iY + iZ != 0)
		{
			float dX = Mathf.Abs(x - iX);
			float dY = Mathf.Abs(y - iY);
			float dZ = Mathf.Abs(-x - y - iZ);

			if (dX > dY && dX > dZ)
				iX = -iY - iZ;
			else if (dZ > dY)
				iZ = -iX - iY;
		}

		return new CellCoordinates(iX, iZ);
	}

	public static Vector3 ToWorldPosition(int x, int z)
	{
		Vector3 position = new Vector3();
		position.x = (x + (-z * 0.5f)) * (Board.InnerRadius * 2f);
		position.z = -z * (Board.OuterRadius * 1.5f);
		return position;
	}

	public static Vector3 ToWorldPosition(CellCoordinates coordinates)
	{
		return ToWorldPosition(coordinates.X, -coordinates.Z);
	}
	#endregion
}
