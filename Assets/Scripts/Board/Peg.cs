﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Peg : MonoBehaviour 
{
	#region Inspector
	[SerializeField] private Color _moveableColor;
	[SerializeField] private Color _hoverColor;
	[SerializeField] private float _moveAnimationDuration = 0.35f;
	[SerializeField] private MeshRenderer _meshRenderer;
	#endregion

	#region Accessors
	public CellCoordinates Coordinates { get; private set; }
	public List<Cell> ValidCells { get { return _validCells; } }
	#endregion

	private Color _defaultColor;
	private List<Cell> _validCells = new List<Cell>();
	private CellCoordinates[] _moveOffsets = new CellCoordinates[]
	{
		new CellCoordinates(2, 0), new CellCoordinates(-2, 0),
		new CellCoordinates(0, -2), new CellCoordinates(2, -2),
		new CellCoordinates(0, 2), new CellCoordinates(-2, 2)
	};

	public void Initialize(CellCoordinates coordinates)
	{
		this.Coordinates = coordinates;
		_defaultColor = _meshRenderer.material.color;
	}

	public void OnMoved(CellCoordinates newCoordinates)
	{
		Coordinates = newCoordinates;

		Vector3 destination = CellCoordinates.ToWorldPosition(Coordinates.X, -Coordinates.Z);

		transform.DOJump(destination, 2f, 1, _moveAnimationDuration);
	}

	public void OnSelected()
	{
		transform.DOMove(transform.position + Vector3.up, _moveAnimationDuration);
	}

	public void OnDeselected()
	{
		transform.DOMove(CellCoordinates.ToWorldPosition(Coordinates), _moveAnimationDuration);
	}

	public void SetMoveHighlightVisible(bool visible)
	{
		_meshRenderer.material.color = visible ? _moveableColor : _defaultColor;
	}

	public bool CanMove(Board board)
	{
		_validCells.Clear();
		Cell[] neighbors = board.GetCellNeighbors(Coordinates);

		for (int i = 0; i < _moveOffsets.Length; i++)
		{
			Cell neighbor = neighbors[i];
			if (neighbor != null && neighbor.Peg != null)
			{
				Cell targetCell = board.GetCellAtCoordinates(Coordinates + _moveOffsets[i]);
				if (targetCell != null && targetCell.Peg == null)
				{
					_validCells.Add(targetCell);
				}
			}
		}

		return _validCells.Count > 0;
	}

	public bool IsValidCell(CellCoordinates coordinates)
	{
		return _validCells.Find((cell) => cell.Coordinates == coordinates) != null;
	}
}
