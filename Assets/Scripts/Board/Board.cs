﻿using UnityEngine;
using System.Collections.Generic;

public class Board : MonoBehaviour
{
	#region Constants
	public const float OuterRadius = 1f;
	public const float InnerRadius = OuterRadius * 0.866025404f;
	#endregion

	#region Inspector
	[SerializeField] private LevelController _levelController;

	[Header("Prefabs")]
	[SerializeField] private GameObject _cellPrefab;
	[SerializeField] private GameObject _pegPrefab;
	#endregion

	private Dictionary<CellCoordinates, Cell> _cellGrid = new Dictionary<CellCoordinates, Cell>();
	private List<Peg> _pegs = new List<Peg>();
	private CellCoordinates[] _cellNeighborOffsets = new CellCoordinates[]
	{
		new CellCoordinates(1, 0), new CellCoordinates(-1, 0),
		new CellCoordinates(0, -1), new CellCoordinates(1, -1),
		new CellCoordinates(0, 1), new CellCoordinates(-1, 1)
	};

	private void Start()
	{
		int boardSize = 0;
		switch(GameData.Difficulty)
		{
			case 1: boardSize = 4; break;
			case 2: boardSize = 5; break;
			case 3: boardSize = 6; break;
		}

		CreateTriangleBoard(boardSize);
	}

	#region Board creation
	public void CreateTriangleBoard(int size)
	{
		for (int z = 0; z < size; z++)
		{
			for (int x = 0; x < z + 1; x++)
			{
				CreateCell(x, z);

				if (!(z == size - 1 && x == (z + 1) / 2))
					CreatePeg(x, z);
			}
		}
	}

	private void CreateCell(int x, int z)
	{
		var cell = (Instantiate(_cellPrefab, transform) as GameObject).GetComponent<Cell>();
		cell.transform.position = CellCoordinates.ToWorldPosition(x, z);

		cell.Initialize(CellCoordinates.FromWorldPosition(cell.transform.position));
		_cellGrid.Add(cell.Coordinates, cell);
	}

	private void CreatePeg(int x, int z)
	{
		var peg = (Instantiate(_pegPrefab) as GameObject).GetComponent<Peg>();
		peg.transform.position = CellCoordinates.ToWorldPosition(x, z);
		peg.Initialize(CellCoordinates.FromWorldPosition(peg.transform.position));

		_cellGrid[peg.Coordinates].Peg = peg;
		_pegs.Add(peg);
	}
	#endregion

	public void OnPegMoved(Peg peg, CellCoordinates newCoordinates)
	{
		SetDestinationCellsVisible(peg, false);
		SetMoveablePegsVisible(false);

		GetCellAtCoordinates(peg.Coordinates).Peg = null;
		GetCellAtCoordinates(newCoordinates).Peg = peg;

		peg.OnMoved(newCoordinates);

		SetMoveablePegsVisible(true);

		if (_pegs.Count == 1)
			_levelController.OnGameEnded(LevelController.LevelState.Won);
		else if (!AreThereAnyValidMoves())
			_levelController.OnGameEnded(LevelController.LevelState.Lost);
	}

	public void OnPegSelected(Peg peg)
	{
		peg.OnSelected();
		SetDestinationCellsVisible(peg, true);
	}

	public void OnPegDeselected(Peg peg)
	{
		SetDestinationCellsVisible(peg, false);
		peg.OnDeselected();
	}

	public Peg RemovePeg(CellCoordinates coordinates)
	{
		Cell cellWithPeg = GetCellAtCoordinates(coordinates);
		Peg pegToRemove = null;
		if (cellWithPeg != null)
		{
			pegToRemove = cellWithPeg.Peg;
			if (pegToRemove != null)
			{
				cellWithPeg.Peg = null;
				pegToRemove.gameObject.SetActive(false);
				_pegs.Remove(pegToRemove);
			}
		}

		return pegToRemove;
	}

	public void RestorePeg(Peg peg)
	{
		_pegs.Add(peg);
		peg.gameObject.SetActive(true);
		GetCellAtCoordinates(peg.Coordinates).Peg = peg;
	}

	public bool AreThereAnyValidMoves()
	{
		return _pegs.Find(p => p.CanMove(this)) != null;
	}

	public void SetMoveablePegsVisible(bool visible)
	{
		foreach(var peg in _pegs)
		{
			peg.SetMoveHighlightVisible(visible && peg.CanMove(this));
		}
	}

	public void SetDestinationCellsVisible(Peg peg, bool visible)
	{
		var validCells = peg.ValidCells;
		foreach (var cell in validCells)
		{
			cell.SetHighlightVisible(visible);
		}
	}

	#region Accessors
	public Cell GetCellAtPosition(Vector3 position)
	{
		return GetCellAtCoordinates(CellCoordinates.FromWorldPosition(position));
	}

	public Cell GetCellAtCoordinates(CellCoordinates coordinates)
	{
		if (coordinates == null)
			return null;

		Cell cell;
		_cellGrid.TryGetValue(coordinates, out cell);
		return cell;
	}

	public Cell[] GetCellNeighbors(CellCoordinates coordinates)
	{
		Cell[] neighbors = new Cell[_cellNeighborOffsets.Length];
		for(int i = 0; i < _cellNeighborOffsets.Length; i++)
		{
			Cell cell = GetCellAtCoordinates(coordinates + _cellNeighborOffsets[i]);
			if (cell != null)
				neighbors[i] = cell;
		}
		return neighbors;
	}
	#endregion
}
