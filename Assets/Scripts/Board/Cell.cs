﻿using UnityEngine;
using System.Collections;

public class Cell : MonoBehaviour 
{
	#region Inpsector
	[SerializeField] private Color _highlightColor;
	[SerializeField] private MeshRenderer _meshRenderer;
	#endregion

	#region Accessors
	public CellCoordinates Coordinates { get; private set; }
	public Peg Peg { get; set; }
	#endregion

	private Color _defaultColor;

	public void Initialize(CellCoordinates coordinates)
	{
		this.Coordinates = coordinates;

		_defaultColor = _meshRenderer.material.color;
	}

	public void SetHighlightVisible(bool visible)
	{
		_meshRenderer.material.color = visible ? _highlightColor : _defaultColor;
	}
}
