﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelController : MonoBehaviour 
{
	public enum LevelState
	{
		Won,
		Lost
	}

	[Tooltip("Length of the game's timer in seconds")]
	[SerializeField] private float _gameDuration = 180f;
	[SerializeField] private GameUIController _uiController;

	private List<Action> _actions = new List<Action>();
	private int _currentActionIndex = 0;

	private WaitForSeconds _timerDelay = new WaitForSeconds(1f);    // Cache this to avoid creating garbage every second
	private Coroutine _timerCoroutine;

	private void Start()
	{
		_uiController.UpdateTimer(_gameDuration);
		_timerCoroutine = StartCoroutine(TimerUpdate());
	}

	private IEnumerator TimerUpdate()
	{
		float timeLeft = _gameDuration;
		while(timeLeft > 0f)
		{
			yield return _timerDelay;
			timeLeft -= 1f;
			_uiController.UpdateTimer(timeLeft);
		}

		OnGameEnded(LevelState.Lost);
	}

	#region Actions
	public void PerformAction(Action action)
	{
		_actions.Add(action);
		action.Perform();
		_currentActionIndex = _actions.Count - 1;
	}

	public void UndoAction()
	{
		_actions[_currentActionIndex].Undo();
		_currentActionIndex--;
	}
	#endregion

	#region Game Events
	public void OnGameEnded(LevelState state)
	{
		StopCoroutine(_timerCoroutine);
		_uiController.OnGameEnded(state);
	}
	#endregion
}
