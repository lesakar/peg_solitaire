﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuUIController : MonoBehaviour 
{
	public void OnDifficulty(int difficulty)
	{
		GameData.Difficulty = difficulty;

		SceneManager.LoadScene("game");
	}
}
