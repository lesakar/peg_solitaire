﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class GameUIController : MonoBehaviour 
{
	[SerializeField] private GameObject _gameOverPanel;
	[SerializeField] private Text _gameOverPanelStateText;
	[SerializeField] private Text _timerText;

	private void Start()
	{
		_gameOverPanel.SetActive(false);
	}

	public void OnGameEnded(LevelController.LevelState state)
	{
		_gameOverPanelStateText.text = string.Format("You {0}!", state == LevelController.LevelState.Won ? "Won" : "Lost");
		_gameOverPanel.SetActive(true);
		_gameOverPanel.transform.DOScale(0f, 0.35f).From();
	}

	public void UpdateTimer(float secondsRemaining)
	{
		int minutes = (int)secondsRemaining / 60;
		int seconds = Mathf.RoundToInt(secondsRemaining % 60);

		if (minutes == 0 && seconds <= 10)
			_timerText.DOColor(Color.red, 0.7f).From();

		_timerText.text = string.Format("{0}:{1:D2}", minutes, seconds);
	}

	#region Button events
	public void OnNewGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void OnMenu()
	{
		SceneManager.LoadScene("menu");
	}
	#endregion
}
